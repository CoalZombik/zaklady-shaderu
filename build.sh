#!/bin/bash

cd tex

for i in {0..2}
do
	echo "==========="
	echo "Iteration $i"
	echo "==========="
	pdflatex -halt-on-error -interaction=nonstopmode -output-directory=.. main.tex > "../$i.log"
	returncode=$?
	cat "../$i.log"
	if [ $returncode -ne 0 ]; then
		exit 1
	fi
	grep "No file.*\.tex" "../$i.log" && exit 1
done

exit 0

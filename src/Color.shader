Shader "zaklady-shaderu/Color"
{
	Properties
	{
		_Color ("Barva", Color) = (1,1,1,1)
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0

            #include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
			};

            struct v2f
            {
                float4 pos : POSITION;
            };

            v2f vert (appdata input)
            {
                v2f output;
				output.pos = UnityObjectToClipPos(input.pos); //Převede lokální pozici na clip
                return output;
            }

			fixed4 _Color;

            fixed4 frag (v2f input) : COLOR
            {
                return _Color; //Vrátí nastavenou barvu
            }
            ENDCG
        }
    }
}

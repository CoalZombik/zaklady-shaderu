Shader "zaklady-shaderu/Phong"
{
	Properties
	{
		_Color ("Barva", Color) = (1,1,1,1)
		_MainTex ("Textura", 2D) = "white"
		_SpecularIntensity ("Intenzita odrazu", Float) = 0.8
		_Shininess ("Lesklost materialu", Float) = 64
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0

            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				float3 normal : NORMAL;
            };

            v2f vert (appdata input)
            {
                v2f output;
				output.pos = UnityObjectToClipPos(input.pos);
				output.uv = input.uv;
				output.viewDir = normalize(mul(unity_ObjectToWorld, input.pos) - _WorldSpaceCameraPos);
				output.normal = UnityObjectToWorldNormal(input.normal);
                return output;
            }

			fixed4 _Color;
			sampler2D _MainTex;
			fixed _SpecularIntensity;
			half _Shininess;

            fixed4 frag (v2f input) : COLOR
            {
				fixed4 color = tex2D(_MainTex, input.uv) * _Color;

				fixed4 ambient = unity_AmbientSky * color;

				fixed4 diffuse = max(0.0, dot(input.normal, _WorldSpaceLightPos0.xyz)) * color * _LightColor0;

				float3 reflectDir = reflect(_WorldSpaceLightPos0.xyz, input.normal);
				fixed4 specular = _LightColor0 * _SpecularIntensity * pow(max(0.0, dot(reflectDir, input.viewDir)), _Shininess);

				return ambient + diffuse + specular;
			}
            ENDCG
        }
    }
}

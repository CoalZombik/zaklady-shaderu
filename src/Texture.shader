Shader "zaklady-shaderu/Texture"
{
	Properties
	{
		_Color ("Barva", Color) = (1,1,1,1)
		_MainTex ("Textura", 2D) = "white"
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0

            #include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0; //UV data
			};

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert (appdata input)
            {
                v2f output;
				output.pos = UnityObjectToClipPos(input.pos);
				output.uv = input.uv; //Předat UV
                return output;
            }

			fixed4 _Color;
			sampler2D _MainTex;

            fixed4 frag (v2f input) : COLOR
            {
                fixed4 color = tex2D(_MainTex, input.uv);
                return color * _Color;
            }
            ENDCG
        }
    }
}

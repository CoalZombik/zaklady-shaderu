Shader "zaklady-shaderu/Shadows"
{
	Properties
	{
		_Color ("Barva", Color) = (1,1,1,1)
		_MainTex ("Textura", 2D) = "white"
		_SpecularIntensity ("Intenzita odrazu", Float) = 0.8
		_Shininess ("Lesklost materialu", Float) = 64
	}

	SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }
        LOD 100

        Pass
        {
			Tags { "LightMode"="ForwardBase" }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 normal : NORMAL;
				LIGHTING_COORDS(3,4)
				fixed3 vertexLight : COLOR0;
            };

            v2f vert (appdata v)
            {
                v2f output;
				output.pos = UnityObjectToClipPos(v.vertex);
				output.uv = v.uv;
				output.worldPos = mul(unity_ObjectToWorld, v.vertex);
				output.viewDir = normalize(output.worldPos - _WorldSpaceCameraPos);
				output.normal = UnityObjectToWorldNormal(v.normal);
				TRANSFER_VERTEX_TO_FRAGMENT(output);

				output.vertexLight = fixed3(0, 0, 0);
				#ifdef VERTEXLIGHT_ON
				[unroll] for(int i = 0; i < 4; i++)
				{
					float3 lightDir = float3(unity_4LightPosX0[i], unity_4LightPosY0[i], unity_4LightPosZ0[i]) - output.worldPos;
					float attenuation = 1.0 / (1.0 + unity_4LightAtten0[i] * dot(lightDir, lightDir));
					lightDir = normalize(lightDir);
					output.vertexLight += attenuation * max(0.0, dot(output.normal, lightDir)) * unity_LightColor[i];
				}
				#endif
                return output;
            }

			fixed4 _Color;
			sampler2D _MainTex;
			fixed _SpecularIntensity;
			half _Shininess;

            fixed4 frag (v2f input) : COLOR
            {
				fixed4 color = tex2D(_MainTex, input.uv) * _Color;
				UNITY_LIGHT_ATTENUATION(attenuation, input, input.worldPos);

				//if(_WorldSpaceLightPos0.w == 0)
				#ifdef DIRECTIONAL
					float3 lightDir = _WorldSpaceLightPos0.xyz;
				#else
					float3 lightDir = normalize(_WorldSpaceLightPos0.xyz - input.worldPos);
				#endif

				fixed4 ambient = unity_AmbientSky * color;

				fixed4 diffuse = max(0.0, dot(input.normal, lightDir)) * color * _LightColor0 * attenuation;

				float3 reflectDir = reflect(lightDir, input.normal);
				fixed4 specular = _LightColor0 * _SpecularIntensity * pow(max(0.0, dot(reflectDir, input.viewDir)), _Shininess) * attenuation;

				return ambient + diffuse + specular + fixed4(input.vertexLight, 1) * color;
			}
            ENDCG
        }

		Pass
        {
			Tags { "LightMode"="ForwardAdd" }
			Blend One One

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_fwdadd_fullshadows

            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

            struct v2f
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
				float3 viewDir : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 normal : NORMAL;
				LIGHTING_COORDS(3,4)
            };

            v2f vert (appdata v)
            {
                v2f output;
				output.pos = UnityObjectToClipPos(v.vertex);
				output.uv = v.uv;
				output.worldPos = mul(unity_ObjectToWorld, v.vertex);
				output.viewDir = normalize(output.worldPos - _WorldSpaceCameraPos);
				output.normal = UnityObjectToWorldNormal(v.normal);
				TRANSFER_VERTEX_TO_FRAGMENT(output);
                return output;
            }

			fixed4 _Color;
			sampler2D _MainTex;
			fixed _SpecularIntensity;
			half _Shininess;

            fixed4 frag (v2f input) : COLOR
            {
				fixed4 color = tex2D(_MainTex, input.uv) * _Color;
				UNITY_LIGHT_ATTENUATION(attenuation, input, input.worldPos);

				#ifdef DIRECTIONAL
					float3 lightDir = _WorldSpaceLightPos0.xyz;
				#else
					float3 lightDir = normalize(_WorldSpaceLightPos0.xyz - input.worldPos);
				#endif

				fixed4 diffuse = max(0.0, dot(input.normal, lightDir)) * color * _LightColor0 * attenuation;

				float3 reflectDir = reflect(lightDir, input.normal);
				fixed4 specular = _LightColor0 * _SpecularIntensity * pow(max(0.0, dot(reflectDir, input.viewDir)), _Shininess) * attenuation;

				return diffuse + specular;
			}
            ENDCG
        }

		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}

\section{Dodatečné mapy}
\subsection{Normálové mapa}
Normálová mapa reprezentuje normály objektu vztažené k~tzv. tangent prostoru -- jedná se o~\uv{deformovaný} prostor, ve~kterém platí, že pro každý bod objektu leží tangenta ve~vodorovné rovině $\pi$ (půdorysně) a~normála je na tuto rovinu kolmá. Odchýlením normály od výchozí hodnoty $(0, 0, 1)$ se odchýlí normála určitého bodu, čímž se změní výsledné osvětlení bez nutnosti existence dalšího vrcholu. Díky normál mapě lze vytvořit iluzi vysoce detailního objektu s~nižšími hardwarovými nároky, než u~vysoce detailního modelu. Vzhledem k~tomu, že zde nedochází k~přímé manipulaci s~vrcholy objektu, nelze touto metodou vytvářet iluzi velkých posunů hran, protože při pohledu ze~strany bude vidět tvar samotného objektu.

Standardně se normálové mapy reprezentují pomocí barevného obrázku, kde každá barevná složka určuje jednu složku vektoru -- červená $x$, zelená $g$ a modrá $z$. Složky vektoru mohou nabývat hodnot od -1 do 1 (kromě $z$, které je může nabývat hodnot pouze od 0 do 1) a~při ukládání do textury je potřeba provést zabalení: $c = (x + 1) / 2$.~\cite{normal_map}

Pro úsporu místa se většinou při výsledném sestavení převedou normálové mapy do formátů obsahující pouze dvě složky vektoru (z~důvodů přesnosti $x$ a $y$) a~třetí složka se za běhu v~shaderu dopočítává pomocí Pythagorovy věty.

Pro získání výsledné normály bodu je nutné nejdříve získat transformační matici z~tangent prostoru do světového prostoru. V~tomto případě se jedná o~vynásobení složek odpovídajícím světovým vektorem, pro $x$ se jedná o~tangentu, pro $z$ se jedná o~normálu a~pro $y$ se jedná o~binormálu -- vektor kolmý na normálu i~tangentu (získaný vektorovým součinem).

V~\code{Properties} přibude další proměnná \code{[Normal] _NormalMap ("Normal mapa", 2D) = "bump"}. Atribut \code{[Normal]} udává, že se jedná o~normal mapu a~výchozí hodnota \code{bump} udává texturu nezměněných normál $(0, 0, 1)$.

Uvnitř samotného kódu je nejdříve potřeba předat patřičnou tangentu do~fragment funkce. Proto si přidáme jak do~struktury \code{appdata}, tak do~\code{v2f} další proměnnou: \code{float4 tangent : TANGENT}. Tato proměnná, na rozdíl od~ostatních vektorů, obsahuje kromě standardních třech os vektoru i~čtvrtou složku $w$, která může nabývat pouze hodnot 1 nebo -1 a~určuje, jestli je potřeba otočit binormálu. Ve~vertex funkci přidáme \code{output.tangent = float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w)}. Zde máme makro \code{UnityObjectToWorldDir}, které transformuje vstupní směrový vektor do~světové pozice. Následně musíme vložit i~netransformovanou čtvrtou složku.

\vspace*{0.5em}
\lstinputlisting[firstnumber=83,firstline=83,lastline=87]{Normal.shader}
\newpage

V~fragment funkci nejdříve získáme patřičnou normálu z~normal mapy. Zde se nachází další makro \code{UnpackNormal}, které podle typu komprese normal mapy rozbalí hodnotu. Následně vypočítáme binormálu, kterou násobíme již zmíněnou $w$ složkou tangent vektoru a~ještě navíc hodnotou \code{unity_WorldTransformParams.w} nabývající také hodnot 1 nebo -1 podle toho, jestli objekt má zápornou škálu způsobující zrcadlení. Na závěr si vytvoříme transformační matici, kterou vynásobíme vektor z~normal mapy. 

\imagecolumn{Suzanne_NormalMap}{fig_suzanne_normalmap}{Vstupní normal mapa}{NormalMap}{fig_normalmap}{Výsledek NormalMap.shader}

\subsection{Sekundární mapy}
Pokud by jste chtěli udělat jednu texturu s~velmi vysokými detaily, dostanete se za~chvíli do~problému s~velikostí GRAM grafické karty. Proto má také například Unity ve~výchozím nastavení zmenšování všech textur na maximální velikost hrany 2048~px. Ale v~mnoha případech by se hodilo zohlednit malé detaily. Na to jsou zde sekundární mapy. Jedná se o~textury, které se budou vícekrát opakovat na~objektu a~zabírat menší plochu vůči své velikosti.

Například hlavní textura zobrazuje podstatný vzhled obličeje a~sekundární textura přidá póry.~\cite{detail_maps}

V~\code{Properties} musíme přidat tři proměnné: textury \code{_DetailTex}, \code{_DetailNormalMap} a~ koeficient \code{_DetailStrength}. Teď je na místě vysvětlit další atribut -- \code{[NoScaleOffset]}. Nastaví chování, aby se v~enginu nezobrazovala volba škály a~posunu. Doteď jsme u~\code{_MainTex} a~\code{_NormalMap} tento atribut neměli a~tyto volby jsme ignorovali, jenže pro detaily již budeme škálu a~posun používat. A bylo by tedy vhodné poskytnout možnost pouze v~podporovaných proměnných, proto \code{[NoScaleOffset]} přidáme ke všem ostatním texturám kromě \code{_DetailTex}.

Při přidávání proměnných do~samotného kódu je potřeba zadefinovat ještě proměnnou \code{float4 _DetailTex_ST} reprezentující již zmíněnou škálu ($xy$) a~posun ($zw$), takže výsledné UV je \code{input.uv * _DetailTex_ST.xy + _DetailTex_ST.zw}, což přesně dělá makro \code{TRANSFORM_TEX}.

Získáme hodnoty z~\code{_DetailTex} a~\code{_DetailNormalMap}. U~barvy stačí udělat lineární interpolaci (funkce \code{lerp}) mezi hodnotou hlavní textury a~hodnotou součinu hlavní textury s~texturou detailní. Jako koeficient interpolace zde máme měnitelnou proměnnou \code{_DetailStrength}. S~normal mapou to bude složitější, nejdříve provedeme lineární interpolaci mezi výchozí normálou $(0, 0, 1)$ a~hodnotou detailní normal mapy, poté je potřeba spojit dvě normály do~sebe. Od toho je zde funkce \code{BlendNormals}, která souřadnice $x$ a~$y$ sečte a~$z$ vynásobí.

Nyní již můžeme pracovat s~výslednými hodnotami stejným způsoben jako v~předchozím příkladech. Nevýhoda této implementace je, že detailní mapu aplikuje na~celý objekt rovnoměrně a~v~našem případě mění vzhled například i~očí. Zpravidla se tento problém řešen více materiály nebo dodatečnou mapou určující sílu detailních textur.

\imagecolumn{DetailNormalMap}{fig_detail_normalmap}{Detailní normal mapa}{DetailMaps}{fig_detailmaps}{Výsledek DetailMaps.shader}

\subsection{Mapa odrazivosti}
Obecně je možné jakoukoliv proměnnou měnící vlastnost materiálu vyjadřovat pomocí určité mapy. V~tomto případě si ukážeme použití mapy určující odrazivost povrchu.

Jedná se o~stále stejný princip. Zadefinování další proměnné v~\code{Properties} a v~samotném kódu. Získání hodnoty z~textury pro určitý pixel pomocí funkce \code{tex2d} a~následné vynásobení výsledku specular výpočtu touto hodnotou. Protože zde požadujeme pouze jednu složku textury, je dobré v~enginu nastavit, aby použil správnou komprimační metodu, která pracuje s~jednou barevnou složkou, aby komprimovaná verze mohla zabírat menší velikost v~GRAM.
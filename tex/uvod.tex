\section{Úvod}
Ať už hrajete videohry, sledujete nejnovější seriály nebo například plánujete novou kuchyň, jistě jste se setkali se~shadery, jen jste o~tom třeba neměli tušení. Bez nich by v~dnešní době nedošlo k~vykreslení žádné 3D (někdy i~2D) scény do výsledného rastrového obrázku. Shadery jsou právě ony skripty, které nejen zpracovávají modely, také se mnohdy používají k~grafickým úpravám výsledných obrázků.

V následujících stránkách se budeme zabývat základy práce se~shadery, vysvětlíme si základní model osvětlení a na závěr krátce naznačíme rozšiřující možnosti, které poskytuje práce s tzv. mapami.
\subsection{Reprezentace 3D scény}
V~3D scéně se mohou vyskytovat dva typy prvků z~hlediska vykreslování: modely a~světla.

\begin{itemize}
\item \textbf{Modely} jsou mnohostěny skládající se z~bodů a~stěn -- trojúhelníků. Hrany se obvykle neukládají, protože na vykreslování nemají žádný vliv. U~stěn se ukládá pouze jejich existence (trojice bodů) a~všechny podstatné informace se přiřazují k~bodům -- normály, tangenty, UV, atd. (postupně popsáno na příkladech).
\item \textbf{Samotná světla} nejsou jinak vykreslována do~scény, ale ovlivňují vzhled modelů svými světelnými vlastnostmi.
\end{itemize}

\subsection{Postupný proces vykreslování}\label{proces_vykreslovani}
Aby se z~3D scény stal výsledný obrázek, je potřeba projít procesem vykreslování, který se skládá z~následujících kroků: \cite{pipeline}
\subsubsection{Vertex shader}
Nejprve je potřeba pro každý bod provést vertex shader, jehož hlavním cílem je převést pozici bodu do~clip prostoru, který bude využíván v~následující fázi. Druhotným cílem je připravit potřebné proměnné pro samotné vykreslování ve fragment shaderu (viz \ref{vykreslovani_fragment}).
\subsubsection{Geometry shader}
Následně může přijít na řadu geometry shader, který má za úkol pozměnit geometrii objektu, tj.~přidat nebo odebrat určité body. Spolu s~ním se může provádět ještě tzv.~tesselace, která každou stěnu nahradí více stěnami přidáním dodatečných bodů (např.~pomocí Catmull-Clark algoritmu). Těmito funkcemi se zde zabývat nebudeme, protože se jedná o~poměrně komplexní problémy.
\subsubsection{Clipping}\label{clipping}
V~clippingu se využije výstup z~vertex shaderu a~vyhodnotí se podle clip pozice, jestli je daný bod mimo rozsah kamery a~jestli se tedy má vyřadit z~následného vykreslování. Clip pozice reprezentuje pozici z~pohledu kamery v~rozsahu podle vykreslovacího jádra. Pro OpenGL se jedná o rozsah (<-1, 1>), pro DirectX o <0, 1>.~\cite{clip_opengl_directx}
\subsubsection{Rasterizace}
Velice důležitá část vykreslování, ve které dochází k~převodu všech stěn na~fragmenty odpovídající pixelu výsledného obrázku (výjimkou je multisampling). Pro každý fragment dochází k~interpolaci výstupních hodnot z~vertex shaderu.
\subsubsection{Fragment shader}\label{vykreslovani_fragment}
Zde dochází k~samotnému vykreslování, kde pro každý fragment dojde k~vypočítání výsledné barvy na~základně vstupních textur a~dat z~vertex shaderu.
\subsubsection{Výsledná kompozice obrázku}
Na závěr dojde k~složení všech dat z~fragment shaderů a~následného post-processingu, který pracuje s~vykreslenou scénou jako s~obrázkem.
\image{RenderingPipeline}{fig_pipeline}{Proces vykreslování~\cite{pipeline}}

\subsection{Shader model}\label{targets}
Před tím, než se zaměříme na~samotné programování, je potřeba si vysvětlit, co znamená shader model. Jedná se o~specifikaci určující možnosti (podporované funkce a~vlastnosti) grafické karty. U~stolních počítačů takřka nenastává problém s~nekompatibilním hardwarem, ale grafické čipy v~mobilních telefonech zdaleka nepodporují pokročilejší vlastnosti, proto je potřeba vhodně zvolit správné nastavení. Pomocí této specifikace lze ověřit, zda je možné daný shader vykreslovat. 

\subsection{Syntaxe}\label{syntaxe}
Vzhledem k~tomu, že uvedené příklady jsou určené pro herní engine Unity, všechny příklady budou psány v~jazyce ShaderLab. Většina kódu bude kompatibilní i~s~konkurenčními produkty, jen bude nutné provést některé změny.

Jazyk ShaderLab se skládá z~dvou částí. První část nastavuje chování herního enginu, ta druhá je samotný shader napsaný v~jazyce CG nebo HLSL, které jsou velice podobné jazyku C a~jsou mezi sebou skoro zaměnitelné.~\cite{cg_vs_hlsl}

\lstinputlisting{Color.shader}

\subsubsection{Nastavení shaderu}

Celý obsah obaluje tag \code{Shader}. Jeho jediným parametrem je název shaderu, který se bude zobrazovat v~uživatelském rozhraní enginu. V~něm se mohou vyskytovat tagy \code{Properties, SubShader, Fallback, CustomEditor}. Ty nejdůležitější jsou \code{Properties} a \code{SubShader}.~\cite{shaderlab_syntax}

V~\code{Properties} se na samostatném řádku nacházejí proměnné shaderu, které lze poté ovládat pomocí uživatelského rozhraní nebo pomocí skriptů. Tyto proměnné se definují ve~tvaru \code{nazev_promenne ("zobrazovany_nazev", typ_promenne) = hodnota}. Možné typy proměnných si postupně ukážeme na~příkladech.

Poté tu je \code{SubShader} ve~kterém se nalézá zbytek kódu. Tento tag se může v~jednom souboru nacházet vícekrát a~při výsledném vykreslování se použije první \code{SubShader} kompatibilní s daným zařízením (viz \ref{targets}).

Zde se poté nachází \code{Tags} ve~kterém se specifikuje pořadí a~typ vykreslování, \code{Pass} obsahující samotný shader a~případně nastavení např.~překrývání, mísení průhlednosti.

\subsubsection{CG}

Samotný CG kód je obalem direktivou \code{CGPROGRAM} a~\code{ENDCG}. Jak již bylo zmíněno výše, tento kód se velice podobá jazyku C. Hlavní typy proměnných, které budeme následně používat jsou tyto:~\cite{shader_datatypes}
\begin{itemize}
\item int -- 32~bitové celé číslo, nemusí být hardwarově vždy podporováno a~může být emulováno složitými výpočty
\item fixed -- 11~bitové fixní reálné číslo s~rozsahem od <-2;2) a~přesností $\frac{1}{256}$
\item half -- 16~bitové reálné číslo s~plovoucí desetinnou čárkou s~rozsahem cca. $\pm 60000$ a~přesností na 3 platná místa
\item float -- 32~bitové reálné číslo s~plovoucí desetinnou čárkou, stejné jako v běžných programovacích jazycích
\item sampler2d -- reprezentuje 2D texturu
\end{itemize}
Existují zde také varianty vektorů (které mohou reprezentovat také barvu) a~matic ve formátu například \code{fixed3} nebo \code{float4x4}.

Moderní grafické karty počítačů provádějí všechny výpočty s~vysokou přesností (\code{float}), i když se jedná o~méně přesný datový typ. Ale u~mobilních grafických karet dává smysl používat datové typy s~nižší přesností, zde může docházet k~razantnímu zlepšení výkonu a~snížení spotřeby.

Na začátku je potřeba definovat, jaké funkce reprezentují vertex a~fragment kód -- \code{#pragma vertex nazev_funkce} a~\code{#pragma fragment frag}. Také je vhodné určit, pro jaký shader target je tento kód určen -- \code{#pragma target cislo_targetu}. Pak přichází na řadu include přídavných souborů. Ve~většině případů stačí \code{#include "UnityCG.cginc"}, kde jsou obsaženy základní důležité funkce. V~určitých případech je vhodné si vytvořit vlastní přídavný soubor, například pokud je stejný kód využíván ve~více shaderech -- syntaxe je totožná s~hlavičkovými soubory jazyku C, až na výjimku, že je zde místo pouhých definic funkcí i~jejich obsah.

Dále je vhodné si zadefinovat patřičné struktury, protože vstupy a~výstupy vertex a~fragment funkcí obsahují více proměnných. Zde si můžete všimnout asi největšího rozdílu oproti jazyku C -- za definicí proměnné se vyskytuje dvojtečka a~následně označení, co se v~této proměnné bude uloženo. Bez tohoto určení by compiler nevěděl, co do proměnné má vložit a~jak s~ní operovat při rasterizaci. Takového označení můžete vidět i~u~funkce \code{frag}.

Jsou zde dva důležité výstupy pro každý shader -- vertex funkce musí mít mezi svými výstupy pozici (označení \code{POSITION}) a~fragment funkce musí vracet pouze barvu (označení \code{COLOR}).

Abyste mohli používat nastavitelné vstupy z~\code{Properties}, je potřeba je zadefinovat jako globální proměnné se stejným názvem. 

Na závěr jsou uvedeny veškeré funkce, jejichž implementací se budeme ve~zbytku textu zabývat.
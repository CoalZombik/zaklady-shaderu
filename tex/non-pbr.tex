\section{Úplné základy}
\subsection{Jednoduchá barva}
Naprostým základem je vykreslení jednotné barvy na objekt. Zdrojový kód tohoto shaderu je uvedený v~kapitole \ref{syntaxe}, proto ho zde nebudu přikládat znovu a~budu citovat pouze důležité části.

Nejdůležitější řádek je \code{output.pos = UnityObjectToClipPos(input.pos)}. Jak jsme si už zmínili v~kapitole \ref{clipping}, je potřeba lokální pozici bodů objektu převést na~clip pozici vůči kameře, aby se mohl objekt v~dalším kroku rasterizovat. Pokud byste se podívali na~zdrojové kódy vestavěných shaderů, zjistíte, že je to inline funkce, která vrací \code{mul(UNITY_MATRIX_VP, mul(unity_ObjectToWorld, float4(pos, 1.0)))}~\cite{buildin_shaders}, aneb postupnou transformaci do~světové pozice a~následnou transformaci kombinované matice pohledu a~projekce.

Transformovanou pozici poté vracíme z~vertex funkce jako \code{POSITION}.

Barvu tomuto objektu nastavujeme v~fragment funkci, kde vracíme \code{return _Color}. Tuto barvu jsme si zadefinovali v~\code{Properties}, abychom ji mohli měnit. Také jsme ji museli určit jako globální proměnnou (řádek 39).

Výsledek můžete vidět na~obrázku~\ref{fig_color}.

\image{Color}{fig_color}{Výsledek Color.shader}

\subsection{Textura}
Jedna barva na celém objektu může být sice pěkná, ale většinou je potřeba mít nějakou texturu. K~tomu si budeme muset nejdříve vysvětlit, co to jsou UV souřadnice.
\subsubsection{UV mapovaní}
Aby bylo specifikováno, jak se textura aplikuje na~model, je potřeba určit pro každý bod souřadnice na~textuře. Graficky se zobrazuje UV mapou -- na~obrázku \ref{fig_suzanne_uv} je UV mapa modelu Suzanne (z~programu Blender).

\newpage
\subsubsection{Implementace shaderu}

\lstinputlisting[firstnumber=23,firstline=23,lastline=50]{Texture.shader}

\imagecolumn{Suzanne_UV}{fig_suzanne_uv}{UV mapa Suzanne}{Suzanne_Texture}{fig_suzanne_texture}{Textura Suzanne}

V~\code{Properties} jsme si přidali další definici: \code{_MainTex ("Textura", 2D) = "white"}. Typ \code{2D} určuje, že se jedná o~2D texturu.

V~kódu si nejdříve přidáme novou proměnnou do struktur, pojmenujeme ji \code{uv} a~určíme, že je \code{TEXCOORD0}. Poté ve~vertex funkci jenom předáme dál. V~fragment funkci voláme \code{tex2D(_MainTex, input.uv)}, která získá patřičnou barvu z~textury. Tu vynásobíme s~volitelnou barvu a~výsledek vrátíme.

\image{Suzanne_Output}{fig_texture}{Výsledek Texture.shader}

\subsection{Phongův osvětlovací model}
Phongův osvětlovací model je empirický model lokálního osvětlení modelu, který se používá jako základní osvětlení takřka v~každém shaderu. Skládá se z~ambientního, difúzního a specular světla.~\cite{phong_model}
\subsubsection{Ambientní světlo}
Ambientní světlo se aplikuje na~celý objekt rovnoměrně a~reprezentuje nepřímé světlo z~dálky mimo scénu (například obloha na Zemi).
\subsubsection{Difúzní světlo}
Difúzní světlo je fyzikálně způsobeno zejména vnitřními odrazy od~částeček objektu, kde světlo se částečně odrazí při vstupu do~objektu a~následně prochází malými částečkami, které sami odrážejí část světla do~náhodných směrů (primární rozptýlené světlo). Odražené světlo se před výstupem z~prostředí může znova odrazit a~vytvořit sekundární rozptýlené světlo.~\cite{reflection}

Phongův model tento efekt reprezentuje jako $C_d=C_o \cdot C_l \cdot k_d(\vec{n} \cdot \vec{l})$, kde $\vec{n}$ je normála objektu, $\vec{l}$ směr světla, $k_d$ koeficient difúzní odrazivosti, $C_o$ barva objektu a~$C_l$ barva příslušného světla. Skalární součin vektorů reprezentuje cosinus úhlu dopadajícího světla od~normál, aneb jak přímo světlo svítí. Obvykle se předpokládá s~tím, že $k_d = 1$.
\subsubsection{Specular světlo}
Specular světlo je hlavní odraz světla od~objektu podle pravidla: úhel dopadu je úhel odrazu. Přestože by se takto mělo jednat pouze o~jediný přesný úhel, vzhledem k~vlastnostem materiálu se jedná určitý úzký rozptyl úhlů. Tento proces se reprezentuje jako $C_s=C_l \cdot k_s \cdot (\vec{v} \cdot \vec{r})^n$, kde $\vec{v}$ je vektor směru z~bodu do místa pozorovatele, $\vec{r}$ vektor odrazu (ten lze vypočítat jako $\vec{r}=2 \cdot (\vec{l} \cdot \vec{n}) \cdot \vec{n} - \vec{l}$), $n$ koeficient intenzity lesklosti a~$k_s$ je koeficient specular odrazivosti.

\newpage
\subsubsection{Implementace shaderu}
\lstinputlisting{Phong.shader}

V~\code{Properties} definujeme parametry využívané pro výpočty.

Na~začátku CG programu se nachází přidání dalšího hlavičkového souboru (\code{#include "Lighting.cginc"}), který nám přidává potřebné proměnné.

V~strukturách jsme si přidali pár dalších proměnných. V~\code{appdata} se jedná o~normálu bodu. Normála je vektor směřující kolmo k~rovině stěny a~výsledná normála bodu, která se pak ukládá do~3D modelu, se počítá jako průměr normál přilehlých stěn. V~\code{v2f} jsme přidali nejen normály, ale navíc proměnnou \code{viewDir}, která bude reprezentovat vektor směru ke~kameře. Této proměnné jsme nastavili, že bude obsahovat \code{TEXCOORD1}, přestože se o~koordináty nejedná. Celé to vychází z~požadavku, že každá proměnná musí mít určité označení, a~protože může mít objekt více koordinátů, využijeme toho. Další možností je více barev (\code{COLOR0, COLOR1, COLOR2, ...}), u~nich se ale předpokládá nižší přesnost (většinou \code{fixed}).~\cite{semantics}

Přejdeme k~vertex funkci. Zde nám přibyl výpočet \code{viewDir} -- vynásobení vektoru pozice transformační maticí, které převede pozici do~světových souřadnic, následný rozdíl s~\code{_WorldSpaceCameraPos} reprezentující světovou pozici kamery a~výslednou normalizaci, aby se jednalo o~směrový vektor. Na~dalším řádku je funkce \code{UnityObjectToWorldNormal}, která transformuje normálu do~světové pozice. Protože normála je směrový vektor, její transformace obnáší pouze rotaci, nikoli posun a~škálu, a~proto nemůžeme použít pouhé násobení maticí.

Ve~fragment funkci nalezneme samotný výpočet Phongova osvětlení. Nejdříve ambient, kde využíváme vestavěnou proměnnou \code{unity_AmbientSky}. U~výpočtu difúzního světla je potřeba si povšimnout vestavěné proměnné \code{_WorldSpaceLightPos0}, která i~přes svůj název obsahuje pro směrové světlo směrový vektor (bližší popis v kapitole \ref{light_types}). Výsledný skalární součin (funkce \code{dot}) ořízneme na~nezáporná čísla funkcí \code{max}, která vybere maximální hodnotu z~vstupů, protože nechceme, aby výsledná barva měla záporné hodnoty, které by pak nesprávně ovlivňovali ostatní typy osvětlení. Difúzní barva je závislá na~barvě světla, proto musíme hodnotu vynásobit vestavěnou proměnnou \code{_LightColor0} pocházející z~\code{Lighting.cginc}.

\newpage
Pro výpočet specular osvětlení je potřeba nejdříve získat směr odrazu pomocí funkce \code{reflect}, která obsahuje výše uvedený vzorec pro odraz. Stejně jako u~difúzního osvětlení i~zde odřezáváme výsledek skalárního součinu. Protože specular není závislý na~barvě samotného objektu (jedná se o~čistý odraz), nefiguruje zde proměnná \code{color}.

Jako výslednou barvu pixelu vracíme součet všech tří osvětlení.

\image{Phong}{fig_phong}{Výsledek Phong.shader}

\subsection{Více světel}
V~tomto příkladu budeme používat tzv.~forward vykreslování, které provádí celý proces vykreslování pro každé světlo zvlášť. Druhý způsob (deferred) jde optimalizovanější cestou, ale je komplikovanější na~implementaci, proto ho zde nepoužijeme.

Samotný kód se takřka neliší od~předchozího příkladu, jen obsahuje dvakrát \code{Pass}. První \code{Pass} se liší pouze přidáním \code{Tags { "LightMode"="ForwardBase" }} určující, že se jedná o hlavní světlo.

Druhý \code{Pass} obsahující znovu stejný kód lišící se více změnami. \code{Tags { "LightMode"="ForwardAdd" }} označuje, že se jedná o~přídavné světlo a~\code{Blend One One} nastavuje, aby se barevný výstup sčítal s~existujícími daty. Dále je tady vynecháno ambientní osvětlení -- výsledná barva se skládá pouze s~difúzního a~specular. Ambientní světlo zde není z~důvodu, protože se jedná o~osvětlení, které není závislé na~samotných zdrojích světla a~proto se nepřidává s~každým dalším zdrojem.~\cite{secondpass}

\subsection{Stíny a různé typy zdrojů světla}\label{light_types}
Zatím jsme pracovali pouze se směrovým zdrojem světla reprezentující například slunce. Ale v~realitě existují i~zdroje světla, které nejsou dostatečně daleko od~pozorovatele, a tedy rozdíl úhlu dopadu světla na~povrchy již není zanedbatelný. Protože 3D scény se snaží o~co nejrealističtější reprodukci reality, i~zde je potřeba tento druh světel vytvořit.

Vestavěná proměnná \code{_WorldSpaceLightPos0} má ještě čtvrtou hodnotu, která určuje, jestli se jedná o~směrové světlo ($w = 0$) nebo ne ($w = 1$). Pokud se jedná o~směrové světlo, souřadnice určují směrový vektor světla.~\cite{variables} Jinak určují světovou pozici zdroje světla -- proto tato proměnná má mírně zavádějící název.

\begin{lstlisting}
float3 lightDir = _WorldSpaceLightPos0.xyz;
float attenuation = 1;
if(_WorldSpaceLightPos0.w }= 0)
{
	lightDir = lightDir - worldPos;
	attenuation = 1.0 / (1.0 + dot(lightDir, lightDir));
	lightDir = normalize(lightDir);
}
\end{lstlisting}

Proměnná \code{attenuation} reprezentuje intenzitu světla (v~našem příkladu správně nepřímo kvadraticky závislá na~vzdálenosti -- skalární součin vektoru mezi bodem a světlem) kterou násobíme výsledek difúzního a~specular výsledku.

Bohužel tato implementace má jisté neduhy -- nerespektuje rozsah světla ani vymezený úhel reflektorového světla. Naštěstí to jde lépe, pomocí vestavěných maker. Spočívá to ve~třech (resp. čtyřech) fázích.

Nejdříve do~výstupní vertex resp. vstupní fragment struktury musíme přidat makro \code{LIGHTING_COORDS(a,b)}, kde \code{a} a \code{b} jsou dvě čísla určující volné \code{TEXCOORD} označení, aby se zde mohli přidat potřebné proměnné.

\lstinputlisting[firstnumber=122,firstline=122,lastline=130]{Shadows.shader}

Poté je potřeba do~vertex funkce přidat makro \code{TRANSFER_VERTEX_TO_FRAGMENT(o)}, kde \code{o} označuje název výstupní struktury. Abychom při kompilaci nenarazili na chybu, musíme přejmenovat některé proměnné, protože toto makro je striktní v~jejich pojmenovávání: vstupní struktura se musí jmenovat \code{v} a~musí obsahovat proměnnou \code{v.vertex} obsahující pozici bodu, také výstupní struktura musí obsahovat proměnnou \code{pos}, jak je vidět v ukázce kódu.

\lstinputlisting[firstnumber=132,firstline=132,lastline=142]{Shadows.shader}

Poslední krok je makro v~fragment funkci \code{UNITY_LIGHT_ATTENUATION(t, i, w)}, kde \code{t} je název budoucí proměnné obsahující intenzitu světla, \code{i} název vstupní struktury a~\code{w} světová pozice. Výsledná proměnná (v~našem případě \code{attenuation}) reflektuje všechny nastavení světel, navíc počítá i~se stíny.

\newpage
\lstinputlisting[firstnumber=151,firstline=151,lastline=158]{Shadows.shader}

Aby tento shader stíny i~vrhal, stačí připojit \code{Pass} z~vestavěného shaderu řádkem \code{UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"}.~\cite{examples}

Nesmíme také zapomenout přidat \code{#pragma multi_compile_fwdbase} do \code{ForwardBase} a~\code{#pragma multi_compile_fwdadd_fullshadows} do \code{ForwardAdd}, čímž se zajistí zkompilování více variant shaderu pro určité typy světel. Proto místo podmínky \code{if} při výpočtu směru světla můžeme elegantněji dát kompilační podmínku \code{#ifdef DIRECTIONAL}.

Popis fungování stínu zde do~podrobností nepopíši, protože se jedná o~značně komplikovaný algoritmus. Základem jsou tzv. depth textury -- reprezentují vzdálenost bodů od~pozorovatele -- ze kterých vzniká stínová mapa. Nejdříve se vykreslí depth textura z~pohledu světla, pro směrové světlo se jedná o~ortografickou projekci, pro bodové světlo o~perspektivní. Také se vykreslí depth textura vztažená ke~kameře. Následně se tyto textury porovnají (světelná se převede do~kamerové projekce) a na místech, kde se vzdálenosti neshodují (světlo narazí dříve aneb světelný depth má menší hodnotu) se nastaví v~daném místě shadow mapa na 0, v ostatních místech na 1.~\cite{shadows} Při následném vykreslování scény se z~této shadow mapy čerpá při výpočtu \code{attenuation} proměnné.

Protože výpočet všech světel může být v~některých scénách velice náročný, engine vykresluje jenom ty nejdůležitější. Aby se zajistilo vykreslení i~dalších světel s~minimální ztrátou výkonu, čtyři další nedůležitá bodová světla se mohou vykreslovat pouze vertexově v~\code{ForwardBase}. Do struktury \code{v2f} vložíme \code{fixed3 vertexLight : COLOR0}, přes kterou budeme předávat vypočítané osvětlení z~vertex funkce.

\lstinputlisting[firstnumber=58,firstline=58,lastline=68]{Shadows.shader}

Zde máme další kompilační podmínku \code{#ifdef VERTEXLIGHT_ON} rozhodující, jestli je potřeba nedůležitá světla počítat. Proměnné \code{unity_4LightPosX0}, \code{unity_4LightPosY0} a~\code{unity_4LightPosZ0} obsahují $x$, $y$ a $z$ souřadnici pozice světel, \code{unity_4LightAtten0} faktor intenzity a~\code{unity_LightColor} barvu světel. Výstup fragment funkce bude navíc obsahovat další součet s~\code{fixed4(input.vertexLight, 1) * color}.

Teď je na místě vysvětlit, jak fungují cykly v~shaderech. Obecně není vhodné používat podmínky a~neurčité cykly, protože jsou s~tím spojené nutné skoky v~programu, které mohou zpomalit vykreslování (zejména na~starších grafických kartách). Proto také pro určité možnosti (aktuálně jsme se setkali zatím jen s~různými typy zdrojů světla) se kompilují shadery vícekrát, aby se co nejvíce zamezilo cyklům. Častokrát se cyklus tzv. rozbalí -- provede se při kompilaci a~výsledný shader obsahuje několikrát stejný kód s~nahrazenými indexy konstantami. V~tomto případě, kdy je předem určen počet opakování (a~není to příliš velké číslo), je rozbalení cyklu naprosto ideální. Pomocí \code{[unroll]} před samotným cyklem určíme, že se má provést rozbalení. Pokud by bylo potřeba vynutit nerozbalování cyklu, stačí přidat \code{[loop]}.~\cite{unroll}

\image{Shadows}{fig_shadows}{Výsledek Shadow.shader}